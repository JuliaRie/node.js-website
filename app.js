var express = require('express');
var app = express();
var recipes = require('./data/data.json');

app.use(express.static('public'));
app.use(express.static('node_modules/bootstrap/dist'));

app.get('/', function (req, res) {
  res.render('index.jade', { titel: 'Home'} );
});

app.get('/about', function (req, res) {
  res.render('about.jade', { titel: 'About'});
});
app.get('/portfolio', function (req, res) {
  res.render('portfolio.jade', { 
  	titel: 'Portfolio',
  	recipes: recipes
  });
});
app.get('/team', function (req, res) {
  res.render('team.jade', { titel: 'Team'});
});
app.get('/contact', function (req, res) {
  res.render('contact.jade', { titel: 'Contact'});
});
app.get('/services', function (req, res) {
  res.render('services.jade', { titel: 'Services'});
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});